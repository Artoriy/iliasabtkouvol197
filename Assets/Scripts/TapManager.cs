﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapManager : MonoBehaviour {

    [SerializeField]
    private Ball[] balls = new Ball[5];

    public Vector2 currentMouseLocation { get; private set; }

    private void Update()
    {
        Click();
        UnClick();
        CalculateMousePosition();
    }

    private void Click()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            if (hit.collider != null && hit.collider.gameObject.layer == 8) // Ball clicked
            {
                foreach (var b in balls)
                {
                    if(hit.collider.name == b.name)
                    {
                        b.GetComponent<Ball>().isBallActive = true;
                    }
                }
            }
        }
    }

    private void UnClick()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            foreach (var b in balls)
            {
                b.GetComponent<Ball>().isBallActive = false;
            }
        }
    }

    private void CalculateMousePosition()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        currentMouseLocation = mousePos;
    }

}
