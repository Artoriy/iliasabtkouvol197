﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    [SerializeField]
    private GameObject rope;

	internal bool isBallActive = false;

    private TapManager tapManager;

	private void Start()
    {
        tapManager = FindObjectOfType<TapManager>();
    }

    private void Update()
    {
        NewDirection();
    }

    private void NewDirection()
    {
        if (isBallActive)
        {
            Vector2 direction = tapManager.currentMouseLocation - (Vector2)rope.transform.position;

            rope.transform.right = direction + new Vector2(30, 0);
        }
    }

}
